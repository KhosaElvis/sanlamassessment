﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SanlamAssessment.Models;
using SanlamAssessment.Models.Business;
using SanlamAssessment.Models.FileInput;

namespace SanlamAssessment.Controllers
{
    public class HomeController : Controller
    {
        private readonly IDebitOrderService _debitOrderService;
        public HomeController(IDebitOrderService debitOrderService)
        {
            _debitOrderService = debitOrderService;
        }
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult GetFileInput()
        {
            return View();
        }

        public IActionResult SuccessFileCompute()
        {
            return View();
        }
        [HttpPost]
        public IActionResult GetFileInput(File file)
        {
            if(file.FileInput == null)
            {
                ViewBag.Message = "Please select a file";
                return View();
            }

            try
            {
                if(!file.FileInput.ContentType.Equals("text/xml", StringComparison.CurrentCultureIgnoreCase))
                {
                    ViewBag.Message = "Only XML formant is permitted, please select different file";
                    return View();
                }
                var paths = _debitOrderService.SendDebitOrders(file.FileInput);
                ViewData["paths"] = paths;

                return View();
            }
            catch(Exception ex)
            {
                ViewBag.Message = ex.Message;
                return View();
            }
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
