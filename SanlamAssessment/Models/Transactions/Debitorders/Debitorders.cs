﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace SanlamAssessment.Models.Transactions.Debitorders
{
    [XmlRoot(ElementName = "deduction")]
    public class Deduction
    {
        [XmlElement(ElementName = "accountholder")]
        public string Accountholder { get; set; }
        [XmlElement(ElementName = "accountnumber")]
        public string Accountnumber { get; set; }
        [XmlElement(ElementName = "accounttype")]
        public string Accounttype { get; set; }
        [XmlElement(ElementName = "bankname")]
        public string Bankname { get; set; }
        [XmlElement(ElementName = "branch")]
        public string Branch { get; set; }
        [XmlElement(ElementName = "amount")]
        public string Amount { get; set; }
        [XmlElement(ElementName = "date")]
        public string Date { get; set; }
    }

    [XmlRoot(ElementName = "debitorders")]
    public class Debitorders
    {
        [XmlElement(ElementName = "deduction")]
        public List<Deduction> Deduction { get; set; }
    }
}
