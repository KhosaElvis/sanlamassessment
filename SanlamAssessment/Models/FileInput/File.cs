﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SanlamAssessment.Models.FileInput
{
    public class File
    {
        [Required]
        public IFormFile FileInput { get; set; }
    }
}
