﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace SanlamAssessment.Models.Business
{
    public interface IDebitOrderService
    {
        ICollection<string> SendDebitOrders(IFormFile file);
    }
}