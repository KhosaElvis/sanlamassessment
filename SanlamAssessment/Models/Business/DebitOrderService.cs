﻿using Microsoft.AspNetCore.Http;
using SanlamAssessment.Models.FlatFileStorage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SanlamAssessment.Models.Business
{
    public class DebitOrderService : IDebitOrderService
    {
        private readonly IFileWriterHelper _fileWriter;
        public DebitOrderService(IFileWriterHelper fileWriter)
        {
            _fileWriter = fileWriter;
        }
        public ICollection<string> SendDebitOrders(IFormFile file)
        {
            try
            {

                var response = XMLReaderHelper.XMLReaderHelper.Read(file);
                var filesPathString = _fileWriter.StoreDebitOrders(response);
                return filesPathString;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
