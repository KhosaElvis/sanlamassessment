﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using SanlamAssessment.Models.Transactions.Debitorders;

namespace SanlamAssessment.Models.FlatFileStorage
{
    public class FileWriterHelper : IFileWriterHelper
    {
        private readonly IHostingEnvironment _env;
        public FileWriterHelper(IHostingEnvironment env)
        {
            _env = env;
        }
        static ICollection<string> filesPaths = new List<string>();
        public ICollection<string> StoreDebitOrders(Debitorders response)
        {
            try
            {
                var debitOrders = response.Deduction;
                var sortByBanks = debitOrders.GroupBy(options => options.Bankname);
                foreach (var group in sortByBanks)
                {
                    CreateFileForNewBankForDebitOrdersTransaction(group);
                }
                return filesPaths;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void CreateFileForNewBankForDebitOrdersTransaction(IGrouping<string, Deduction> group)
        {
            try
            {
                var fileName = group.Key;
                var path = _env.ContentRootPath;
                path = string.Format("{0}/FilesDirectory/", path);
                bool exists = Directory.Exists(path);

                if (!exists)
                    Directory.CreateDirectory(path);

                using (StreamWriter file = new StreamWriter(path + fileName + ".txt", true, Encoding.UTF8))
                {
                    filesPaths.Add(path + fileName + ".txt");
                    file.WriteLine(fileName);
                    var sortedDebitOrders = SortDebitOrders(group);
                    foreach (var bank in group)
                    {
                        file.WriteLine(string.Format("{0}\t\t{1}\t\t{2}\t\t{3}\t\t{4}\t\t{5}", bank.Accountholder, bank.Amount, bank.Accountnumber,
                            bank.Accounttype, bank.Branch, bank.Date));
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private ICollection<Deduction> SortDebitOrders(IGrouping<string, Deduction> group)
        {
            try
            {
                List<Deduction> deductions = new List<Deduction>();
                List<Deduction> sortedBySurnameAndAmount = new List<Deduction>();

                Dictionary<string, string> originalNames = new Dictionary<string, string>();

                foreach (var record in group)
                {
                    originalNames.Add(record.Accountnumber, record.Accountholder);
                    record.Accountholder = record.Accountholder.Split(" ")[1];
                    deductions.Add(record);
                }

                var amountAndSurnameSortedDebitOrders = deductions.OrderBy(optionsAmount => optionsAmount.Amount)
                                                                   .OrderBy(options => options.Accountholder);

                foreach (var deduction in amountAndSurnameSortedDebitOrders)
                {
                    foreach (var dictionaryValue in originalNames)
                    {
                        if (deduction.Accountnumber.Equals(dictionaryValue.Key, StringComparison.CurrentCultureIgnoreCase))
                        {
                            deduction.Accountholder = dictionaryValue.Value;
                            sortedBySurnameAndAmount.Add(deduction);
                        }
                    }
                }

                return sortedBySurnameAndAmount;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
