﻿using System.Collections.Generic;
using SanlamAssessment.Models.Transactions.Debitorders;

namespace SanlamAssessment.Models.FlatFileStorage
{
    public interface IFileWriterHelper
    {
        ICollection<string> StoreDebitOrders(Debitorders response);
    }
}