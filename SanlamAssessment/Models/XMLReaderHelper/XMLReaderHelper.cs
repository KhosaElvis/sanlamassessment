﻿using Microsoft.AspNetCore.Http;
using SanlamAssessment.Models.Transactions.Debitorders;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace SanlamAssessment.Models.XMLReaderHelper
{
    public class XMLReaderHelper
    {
        public static Debitorders Read(IFormFile file)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Debitorders));

                using (Stream reader = file.OpenReadStream())
                {
                    return (Debitorders)serializer.Deserialize(reader);
                }
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
