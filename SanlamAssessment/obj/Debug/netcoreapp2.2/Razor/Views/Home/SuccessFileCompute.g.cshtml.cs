#pragma checksum "C:\Users\User\source\repos\SanlamAssessment\SanlamAssessment\Views\Home\SuccessFileCompute.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "a8a1eaf5360e861001fe6e9e4b5c0b206af270f4"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_SuccessFileCompute), @"mvc.1.0.view", @"/Views/Home/SuccessFileCompute.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Home/SuccessFileCompute.cshtml", typeof(AspNetCore.Views_Home_SuccessFileCompute))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\User\source\repos\SanlamAssessment\SanlamAssessment\Views\_ViewImports.cshtml"
using SanlamAssessment;

#line default
#line hidden
#line 2 "C:\Users\User\source\repos\SanlamAssessment\SanlamAssessment\Views\_ViewImports.cshtml"
using SanlamAssessment.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"a8a1eaf5360e861001fe6e9e4b5c0b206af270f4", @"/Views/Home/SuccessFileCompute.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"215557a53442ca5bddcbd77714c3904079cb816f", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_SuccessFileCompute : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 1 "C:\Users\User\source\repos\SanlamAssessment\SanlamAssessment\Views\Home\SuccessFileCompute.cshtml"
   
    var paths = (List<string>)@ViewData["paths"];

#line default
#line hidden
            BeginContext(59, 29, true);
            WriteLiteral("\r\n    <table class=\"table\">\r\n");
            EndContext();
#line 6 "C:\Users\User\source\repos\SanlamAssessment\SanlamAssessment\Views\Home\SuccessFileCompute.cshtml"
          
            if (paths != null)
            {

#line default
#line hidden
            BeginContext(147, 139, true);
            WriteLiteral("                <h5 style=\"color:green;\"> Successfully sent the debit orders to the Banks, follow the paths bellow to see the copies</h5>\r\n");
            EndContext();
#line 10 "C:\Users\User\source\repos\SanlamAssessment\SanlamAssessment\Views\Home\SuccessFileCompute.cshtml"

                foreach (var item in paths)
                {

#line default
#line hidden
            BeginContext(352, 84, true);
            WriteLiteral("                    <tr>\r\n                        <td>\r\n                            ");
            EndContext();
            BeginContext(437, 4, false);
#line 15 "C:\Users\User\source\repos\SanlamAssessment\SanlamAssessment\Views\Home\SuccessFileCompute.cshtml"
                       Write(item);

#line default
#line hidden
            EndContext();
            BeginContext(441, 60, true);
            WriteLiteral("\r\n                        </td>\r\n                    </tr>\r\n");
            EndContext();
#line 18 "C:\Users\User\source\repos\SanlamAssessment\SanlamAssessment\Views\Home\SuccessFileCompute.cshtml"

                }
            }
        

#line default
#line hidden
            BeginContext(548, 14, true);
            WriteLiteral("    </table>\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
